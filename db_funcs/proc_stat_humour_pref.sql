DROP PROCEDURE IF EXISTS stat_humour_pref;
DELIMITER //
CREATE PROCEDURE stat_humour_pref(the_game_id INT)
BEGIN

    SELECT cp.nick AS czar, pp.nick AS responder, ROUND(times_picked*100/times_played) AS perc_picked FROM
        (
            SELECT czars.card_czar_id,
                   gp.player_id,
                   (SELECT COUNT(qh.question_id)
                    FROM question_history qh
                             INNER JOIN response_history rh ON rh.id = qh.winner_id
                    WHERE qh.game_id = gp.game_id
                      AND qh.card_czar_id = czars.card_czar_id
                      AND rh.player_id = gp.player_id) AS times_picked,
                   (SELECT COUNT(qh.question_id)
                    FROM question_history qh
                    WHERE qh.card_czar_id = czars.card_czar_id
                      AND qh.game_id = gp.game_id)     AS times_played
            FROM (
                     SELECT DISTINCT card_czar_id
                     FROM question_history
                     WHERE game_id = the_game_id
                 ) AS czars,
                 game_players gp
            WHERE gp.game_id = the_game_id
              AND gp.player_id != czars.card_czar_id
        ) AS perc_calc
            INNER JOIN player cp ON perc_calc.card_czar_id = cp.id
            INNER JOIN player pp ON perc_calc.player_id = pp.id

    WHERE times_picked/times_played > .5;

end//
DELIMITER ;
