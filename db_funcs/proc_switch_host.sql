DROP PROCEDURE IF EXISTS switch_host;
DELIMITER //
CREATE PROCEDURE switch_host(IN the_game_id INT, IN old_host_id INT)
BEGIN

    DECLARE players_left INT;
    DECLARE should_kill_game BOOLEAN DEFAULT FALSE;
    DECLARE new_host_id INT DEFAULT NULL;

    START TRANSACTION;

    # Remove hostship from old host
    UPDATE game_players SET is_host = FALSE WHERE player_id = old_host_id AND game_id=the_game_id;

    # Get number of host candidates
    SELECT COUNT(player_id) INTO players_left FROM game_players
    WHERE game_id = the_game_id AND player_id != old_host_id AND is_connected = TRUE;

    IF players_left >= 1 THEN

        # Best candidate is the player with the highest score. If there is a tie, it is the player who
        # logged on earliest.
        SELECT gp.player_id INTO new_host_id FROM game_players gp INNER JOIN player p on gp.player_id = p.id
        WHERE is_connected = TRUE AND game_id = the_game_id AND player_id != old_host_id
        ORDER BY gp.points DESC, p.logon_time ASC LIMIT 1;

        UPDATE game_players SET is_host = TRUE WHERE game_id = the_game_id AND player_id = new_host_id;

    end if;

    COMMIT;

    SET should_kill_game = (players_left < 2);

    #output
    SELECT should_kill_game, new_host_id;

end//
DELIMITER ;
