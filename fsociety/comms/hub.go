package comms

import (
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
)

// map of gameId's that point to a map of playerId's that point to their broadcast channels
var connectionHub = make(map[int64]map[int64]chan ServerSideEvent)

// gets the channel associated with the player in a game. Creates and registers it if it doesn't exit.
func GetBCChannel(gameId int64, playerId int64) chan ServerSideEvent {
	playerChans, ok := connectionHub[gameId]
	if !ok {
		playerChans = make(map[int64]chan ServerSideEvent)
		connectionHub[gameId] = playerChans
	}

	channel, ok := playerChans[playerId]
	if !ok {
		channel = make(chan ServerSideEvent)
		playerChans[playerId] = channel
	}

	return channel
}

// Deletes channel from hub. Important so as to not send events to dead channels.
func CloseChannel(gameId int64, playerId int64) {
	if playerChans, ok := connectionHub[gameId]; ok {
		delete(playerChans, playerId)
	}
}

// Sends event to all channels in a game
func Broadcast(gameId int64, event ServerSideEvent, excludes ...int64) {
	if playerChans, ok := connectionHub[gameId]; ok {
		for playerId := range playerChans {
			if !constants.ItemInArr(excludes, playerId) {
				playerChans[playerId] <- event
			}
		}
	}
}

// Send event to one player
func BroadcastSingle(gameId int64, playerId int64, event ServerSideEvent) {
	if playerChans, ok := connectionHub[gameId]; ok {
		if ch, ok := playerChans[playerId]; ok {
			ch <- event
		}
	}
}

// Send error message to one player
func BroadcastError(gameId int64, playerId int64, msg string) {
	BroadcastSingle(gameId, playerId, ServerSideEvent{
		Type: constants.SEV_ERR,
		Data: struct {
			Info string `json:"info"`
		}{msg},
	})
}
