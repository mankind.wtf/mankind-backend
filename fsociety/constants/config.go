package constants

const (
	CFG_HAND_SIZE    = "HAND_SIZE"
	CFG_PLAYER_LIMIT = "PLAYER_LIMIT"
	CFG_ROUND_LIMIT  = "ROUND_LIMIT"
)

var ALL_CFG = []string{
	CFG_HAND_SIZE, CFG_PLAYER_LIMIT, CFG_ROUND_LIMIT,
}

// If you add more config options, set their defaults in /db_funcs/proc_create_game.sql
// If you want value error checking, add that in config/valtest.go
// Also then update the readme :)
