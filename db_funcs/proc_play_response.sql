DROP PROCEDURE IF EXISTS play_response;
DELIMITER //
CREATE PROCEDURE play_response(the_game_id INT, the_player_id INT, the_resp_cards VARCHAR(255))
BEGIN

    DECLARE cur_round INT;
    DECLARE cur_sub_round INT;
    DECLARE this_resp_id INT;
    DECLARE num_commas INT;
    DECLARE num_cards_found INT;
    DECLARE ordering_counter TINYINT DEFAULT 0;

    # 0 fine; 1 move to picking phase
    # -1 card not found; -2 card not player's; -3 player played; -4 player is czar
    DECLARE status_code TINYINT DEFAULT 0;

    # for looping over csv:
    DECLARE tmp_card VARCHAR(128);
    DECLARE tmp_resp_cards VARCHAR(255);

    # get round status
    SELECT round_num, sub_round_num INTO cur_round, cur_sub_round FROM question_history
    WHERE game_id = the_game_id
    ORDER BY round_num DESC, sub_round_num DESC LIMIT 1;

    # check that all cards are legit
    SELECT COUNT(id) INTO num_cards_found FROM response_card WHERE FIND_IN_SET(id, the_resp_cards);
    SET num_commas = CHAR_LENGTH(the_resp_cards)-CHAR_LENGTH(REPLACE(the_resp_cards, ',', ''));

    IF num_cards_found != num_commas+1 THEN # at least one card illegit
        SET status_code = -1;

    ELSE
        # get cards player owns
        SELECT COUNT(response_card_id) INTO num_cards_found
        FROM hands WHERE game_id = the_game_id AND player_id = the_player_id
                     AND FIND_IN_SET(response_card_id, the_resp_cards);

        IF num_cards_found != num_commas+1 THEN # at least one card does not belong to player
            SET status_code = -2;

            # check if player played this sub-round
        ELSEIF (SELECT COUNT(player_id) FROM response_history WHERE game_id = the_game_id AND player_id = the_player_id
            AND round_num = cur_round AND sub_round_num = cur_sub_round) > 0 THEN
            SET status_code = -3;

            # check if player this sub-round's card czar
        ELSEIF (SELECT COUNT(the_player_id) FROM question_history WHERE game_id = the_game_id AND card_czar_id = the_player_id
            AND round_num = cur_round AND sub_round_num = cur_sub_round) > 0 THEN
            SET status_code = -4;

        END IF;
    end if;

    IF status_code = 0 THEN

        START TRANSACTION;

        # insert into resp history
        INSERT INTO response_history (game_id, player_id, round_num, sub_round_num)
        VALUES (the_game_id, the_player_id, cur_round, cur_sub_round);
        SET this_resp_id = LAST_INSERT_ID();

        # loop over cards and insert
        SET tmp_resp_cards = CONCAT(the_resp_cards, '');
        loop_over_cards: LOOP

            SET tmp_card = SUBSTRING_INDEX(tmp_resp_cards, ',', 1);

            # insert card
            INSERT INTO response_history_cards (response_id, response_card_id, ordering)
                VALUES (this_resp_id, tmp_card, ordering_counter);

            SET ordering_counter = ordering_counter + 1;
            SET tmp_resp_cards = MID(tmp_resp_cards, CHAR_LENGTH(tmp_card)+2); # rm part after next comma

            IF tmp_resp_cards = '' THEN
                LEAVE loop_over_cards;
            end if;
        end loop loop_over_cards;

        # Rm card from hand
        DELETE FROM hands WHERE game_id = the_game_id AND player_id = the_player_id
            AND FIND_IN_SET(response_card_id, the_resp_cards);

        COMMIT;

        # check if all players played
        IF everyone_played(the_game_id) THEN
            SET status_code = 1;
        end if;

    end if;

    # output
    SELECT status_code;

end//
DELIMITER ;
