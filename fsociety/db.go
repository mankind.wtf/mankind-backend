package fsociety

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var (
	gDB  *sql.DB
	muDB sync.Mutex
)

// Gets the db handle. Will only create once per program lifetime
// For concurrency, this method can only be called once-at-a-time
func GetDB() *sql.DB {
	muDB.Lock()
	defer muDB.Unlock()

	if gDB != nil {
		return gDB
	}

	host := getEnv("DB_HOST")
	usr := getEnv("DB_USR")
	pwd := getEnv("DB_PWD")
	dbName := getEnv("DB_NAME")

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", usr, pwd, host, dbName))
	if err != nil {
		log.Fatal(err)
	}

	gDB = db

	return gDB
}

func getEnv(key string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		log.Fatal("Fatal: Env var " + key + " not set")
	}
	return val
}
