# Mankind.wtf Backend

This is the backend repo for the Mankind.wtf game platform. It is written to host multiple concurrent multiplayer cards against humanity games.

## Terminology

- Round phase: One third of a sub-round. Either the playing phase (players playing responses face-down),
    the picking phase (the cards are turned face up and the czar picks the winner), or 
    the picked phase (the winner has been picked; waiting for next sub-round).
- Sub-round: A move through the three game phases. One card czar throughout.
- Round: A full move through sub-rounds across the players. 
    One round has *n* sub-rounds, where *n* is the number of players connected to the game.
    Each sub-round has a unique card czar being one of the players.
- Host: Arbitrary host of game. Has some minor privileges/responsibility to the game (see below). 
The host is the first player to connect, and will switch if she disconnects. 
- fsociety: The project codename.

## API Documentation

Gameplay initiation procedure:

- First client creates a game (`POST /game`).
- Other clients register themselves to said game (`POST /game/{id}`).
- All clients connect to websocket endpoint (`/gamestate/{id}`).
- Receive and send messages appropriately (table below).

### POST /game

Create a game

Request:

    {
        "nick": string - creating user's nickname
    }
    
Response:

    {
        "game_id": int - the created game's id
        "player_auth": string - auth token to be used with future requests
    }
    
### POST /game/{game_id}

Register a user to an already-created game

Request:

    {
        "nick": string - creating user's nickname
    }
    
Response:

    {
        "player_auth": string - auth token to be used with future requests
    }
    
### POST /suggestion

Suggest a card

Request:

    {
        "text": string - text on the card
        "question": boolean - true if question card, false if response card
    }
    
Response will be code `201` if successfully submitted.

### WS /gamestate/{game_id}

Websocket for main gameplay. After successful connection is made, other players will be notified. 
To leave the game, just send a closing message at any time. 

Auth will be done once at handshake-time. Include player auth in the query string:

    ?auth=<player_auth>
    
If there is some auth issue or other problem not gameplay related, the server will
respond as if this was a REST request (i.e. not upgrade connection to WS).

Once connected to WS, messages look like this:

    {
        "type": string - message type
        "data": {} - extra relevant data
    }

**Server Side Messages**

Type | Desc | data
--- | --- | ---
`STATUS` | The whole game's state. Sent upon first connect and game completion. Can be requested any time.| \[a lot of dynamic stuff\]
`ERR` | Some issue with client message | `info`
`PLAYER_JOIN` | A player has joined the game. | `nick`
`PLAYER_LEFT` | A player left the game. | `nick`
`NEW_HOST` | Host switched | `nick`
`GAME_KILLED` | The in-progress game has been stopped prematurely. Game completion info will be sent immediately after | `reason`
`ROUND_PLAYING` | Sub-round started (if game was in lobby, game started too) | `card_czar`, `round_num`, `sub_round_num`, `question`
`ROUND_PICKING` | Sub-round moved from playing phase to czar picking phase | `response_detail`
`HAND_UPDATED` | The cards in your hand has updated | `cards`
`RESPONSE_PLAYED` | Someone played a response | n/a
`WINNER_PICKED` | Winner of the sub-round has been picked | `winner_nick`, `response_id`, `rankings`
`GAME_COMPLETE` | Game finished. Game completion data will be sent immediately after | n/a
`CONFIG_UPDATED` | Game config updated | `config`

**Client Side Messages**

Where no data is required, send an empty map (`{}`).

Type | Desc | data
--- | --- | ---
`STATUS_REQ` | Request game state | {}
`HAND_REQ` | Request hand. Only in-game | {}
`START_GAME` | Begin a game. Only the host can do this. | {}
`PLAY_RESPONSE` | Play a response to the question. | `card_ids: int[]`
`PICK_WINNER` | Pick the winning response. Card czar only | `response_id: int`
`SET_CONFIG` | Change game config. In-lobby only, host only. See config section | `config: map<str, str>`
`GET_CONFIG` | Get the game's config | {}
`END_GAME` | Force end the game. Host only | {}
`MOVE_ON` | Force move to next game phase. In-game only, host only. If no responses played, next sub-round will start. | {} 

**Config**

Key | Desc | Default
--- | --- | ---
`HAND_SIZE` | Size of players' hands at playing-time. | 7
`PLAYER_LIMIT` | Max players (whether connected or not) | 6
`ROUND_LIMIT` | Last round number before game ends | 5

**Server-side timers the client should be aware of**

Desc | Duration
--- | ---
After the winner has been picked and before the next sub-round starts | 5 sec
Automatic move to picking phase (if all players do not play responses). Starts after first response played | 1 min 30 sec
Cancellation of picking phase and start of next sub-round (if card czar does not make a decision) | 1 min 30 sec

# Running the server

Big thing to remember with fsociety, is that the mysql database has to be structured and stored procedures must be installed before fsocietyback is run for the first time. If it isn't you'll get an obscure nullpointer exception when trying to create a game. 

Setting up stored functions usually causes mysql to reject them because of some bin logging issue. Easiest solution is to make the fsociety mysql user super (by doing `GRANT SUPER ON *.* TO fsoc`) only for creating the functions. You can make the user non-super right after.

Also, you need some cards in the `question_card` and `response_card` tables.

## Configuring the backend

Environment variables (required):
- `DB_HOST`
- `DB_NAME`
- `DB_USR`
- `DB_PWD`

More config can be set in source in `fsociety/globals/globals.go`

## Testing the backend locally

Cool little interactive CLI app in `test/` dir. More info there.

## Building the backend

See `.gitlab-ci.yml`
