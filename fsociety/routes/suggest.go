package routes

import (
	"encoding/json"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"log"
	"net/http"
)

func suggestCard(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var req struct {
			Text  string `json:"text"`
			Quest bool   `json:"question"`
		}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			comms.WriteFailure(w, 400, err.Error())
			return
		}
		if req.Text == "" {
			comms.WriteFailure(w, 400, "Field 'text' undefined.")
			return
		}

		db := fsociety.GetDB()
		db.Exec("INSERT INTO card_suggestions(text, question) VALUES (?,?)", req.Text, req.Quest)

		comms.WriteJson(w, struct {
			Success bool `json:"success"`
		}{true}, 201)

		if !globals.IsProd() {
			log.Printf("Card suggested")
		}

	} else {
		comms.WriteFailure405(w)
	}
}
