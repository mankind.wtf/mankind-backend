package routes

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/config"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/game"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
)

func gameStateHandler(w http.ResponseWriter, r *http.Request) {
	gameId, err := strconv.ParseInt(mux.Vars(r)["game_id"], 10, 64)
	if err != nil {
		comms.WriteFailure(w, 400, "Cannot parse gameId")
		return
	}

	db := fsociety.GetDB()

	if !game.GameExists(gameId, db) {
		comms.WriteFailure(w, 404, fmt.Sprintf("Game with id %d not found.", gameId))
		return
	}

	uri, _ := url.Parse(r.RequestURI)
	auth := uri.Query().Get("auth")
	if auth == "" {
		comms.WriteFailure(w, 401, "Authorization parameter not specified")
		return
	}
	thePlayer, fail := game.CheckPlayerAuth(auth, gameId, db)
	if fail != nil {
		comms.WriteFail(w, *fail)
		return
	}

	// Upgrade to WS ===
	upgrader := websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	con, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		// connection error. Can't even tell the requester :/
		if !globals.IsProd() {
			log.Print(err)
		}
		return
	}
	defer con.Close()

	// notify others and update db
	game.EventPlayerConnected(gameId, *thePlayer, true, db)

	// setup player_left message
	defer game.EventPlayerConnected(gameId, *thePlayer, false, db)

	// send the game state
	state := game.GetGameState(gameId, db)
	err = comms.SendMessage(con, constants.SEV_STATUS, state)
	if err != nil {
		if !globals.IsProd() {
			log.Print(err)
		}
		return
	}

	// send current config
	err = comms.SendMessage(con, constants.SEV_CONFIG, struct {
		Cfg map[string]string `json:"config"`
	}{config.GetAllConfig(gameId, db)})
	if err != nil {
		if !globals.IsProd() {
			log.Print(err)
		}
		return
	}

	// if in_game, send hand
	if strings.HasPrefix(state.Status, constants.GS_IG_PREFIX) {
		err = comms.SendMessage(con, constants.SEV_HAND_UPDATED, struct {
			Cards []game.ResponseCard
		}{game.GetPlayerHand(gameId, thePlayer.Id, db)})
		if err != nil {
			if !globals.IsProd() {
				log.Print(err)
			}
			return
		}
	}

	// boot up message listener
	clientMessageChan := make(chan game.ClientSideEvent)
	errChan := make(chan error)
	go func() {
		for {
			_, msg, err := con.ReadMessage()
			if err != nil {
				errChan <- err
				break
			}

			event := game.ClientSideEvent{GameId: gameId, Player: *thePlayer}
			err = json.Unmarshal(msg, &event)
			if err != nil {
				errChan <- err
				break
			}

			clientMessageChan <- event
		}
	}()

	// setup BC listening
	bcChannel := comms.GetBCChannel(gameId, thePlayer.Id)
	defer comms.CloseChannel(gameId, thePlayer.Id)

	for { //ever await messages & broadcasts
		select {
		case event := <-clientMessageChan:
			go game.HandleClientEvent(event, db)

		case event := <-bcChannel:
			err := comms.SendMsg(con, event)
			if err != nil {
				errChan <- err
			}

		case err := <-errChan:
			if !globals.IsProd() {
				log.Print(err)
			}
			return
		}
	}

}
