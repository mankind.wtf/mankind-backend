package game

import (
	"database/sql"
	"encoding/json"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/config"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"log"
	"strings"
	"sync"
)

var (
	muCon sync.Mutex
)

type ClientSideEvent struct {
	Type   string
	Data   map[string]json.RawMessage
	GameId int64
	Player Player
}

// Method is synchronised. A lot of stuff happens on connect and disconnect. Cannot have interference.
func EventPlayerConnected(gameId int64, player Player, connected bool, db *sql.DB) {
	muCon.Lock()
	defer muCon.Unlock()

	// Note: if you want to send the player anything on connect, do so in the route method (routes/gamestate.go)
	// This method is executed before the player's broadcasting is set up.

	db.Exec("UPDATE game_players SET is_connected=? WHERE game_id=? AND player_id=?",
		connected, gameId, player.Id)

	// set host if there are none.
	gameHost := GetHostId(gameId, db)
	if connected && gameHost == nil {
		db.Exec("UPDATE game_players SET is_host=TRUE WHERE game_id=? AND player_id=?", gameId, player.Id)
	}

	// Let all other players know ===

	var playerMsg comms.ServerSideEvent
	if connected {
		playerMsg = comms.ServerSideEvent{
			Type: constants.SEV_PLAYER_JOIN,
			Data: map[string]string{"nick": player.Nick},
		}
	} else {
		playerMsg = comms.ServerSideEvent{
			Type: constants.SEV_PLAYER_LEFT,
			Data: map[string]string{"nick": player.Nick},
		}
	}
	comms.Broadcast(gameId, playerMsg, player.Id)

	if !globals.IsProd() {
		var conStr string
		if connected {
			conStr = "connected to"
		} else {
			conStr = "disconnected from"
		}
		log.Printf("Player %d %s game %d", player.Id, conStr, gameId)
	}

	// If disconnect event
	if !connected {
		gameStatus := GetGameStatus(gameId, db)
		gameKilled := false

		// if it was the host that disconnected
		if gameHost != nil && *gameHost == player.Id {
			gameKilled = SwitchHost(gameId, player.Id, db) // handles game killing internally

			// someone else disconnected, but should still kill game
		} else if strings.HasPrefix(gameStatus, constants.GS_IG_PREFIX) && GetActivePlayerCount(gameId, db) < 2 {
			KillGame(gameId, "Too few players connected", db)
			gameKilled = true

		}

		if !gameKilled && strings.HasPrefix(gameStatus, constants.GS_IG_PREFIX) {
			// if disconnector is card czar and game in game (any phase)
			cz, _ := GetCzarAndQuestion(gameId, db)
			if cz.Id == player.Id {
				if !globals.IsProd() {
					log.Printf("Skipping to next sub-round in game %d because Czar disconnected", gameId)
				}
				// whether picking phase or not, this will behave correctly.
				PostWinnerPicking(gameId, db)

				// if DC during play phase, move on if no players left to play
			} else if gameStatus == constants.GS_IG_PLAYING && EveryonePlayed(gameId, db) {

				MoveToPickingPhase(gameId, db)
			}
		}
	}
}

// Handle a client side event/request.
// Run this bad boy in a goroutine.
func HandleClientEvent(event ClientSideEvent, db *sql.DB) {
	switch event.Type {
	case constants.CEV_STATUS:
		comms.BroadcastSingle(event.GameId, event.Player.Id,
			comms.ServerSideEvent{Type: constants.SEV_STATUS, Data: GetGameState(event.GameId, db)})
		break

	case constants.CEV_HAND:
		if IsInGame(event.GameId, db) {
			JustSendHand(event.GameId, event.Player.Id, db)
		} else {
			comms.BroadcastError(event.GameId, event.Player.Id, "Cards only available in-game")
		}
		break

	case constants.CEV_START_GAME:
		StartGame(event.GameId, event.Player.Id, db)
		break

	case constants.CEV_RESPOND:
		if respRaw, ok := event.Data["card_ids"]; ok {
			var resp []int64
			if err := json.Unmarshal(respRaw, &resp); err == nil {
				PlayResponse(event.GameId, event.Player.Id, resp, db)
			} else {
				comms.BroadcastError(event.GameId, event.Player.Id, err.Error())
			}
		} else {
			comms.BroadcastError(event.GameId, event.Player.Id, "Cards to play not specified")
		}
		break

	case constants.CEV_PICK:
		if respRaw, ok := event.Data["response_id"]; ok {
			var respId int64
			if err := json.Unmarshal(respRaw, &respId); err == nil {
				PickWinner(event.GameId, event.Player.Id, respId, db)
			} else {
				comms.BroadcastError(event.GameId, event.Player.Id, err.Error())
			}
		} else {
			comms.BroadcastError(event.GameId, event.Player.Id, "Response id not specified")
		}
		break

	case constants.CEV_WCONFIG:
		if respRaw, ok := event.Data["config"]; ok {
			var cfg map[string]string
			if err := json.Unmarshal(respRaw, &cfg); err == nil {
				ConfigUpdateEvent(event.GameId, event.Player.Id, cfg, db)
			} else {
				comms.BroadcastError(event.GameId, event.Player.Id, err.Error())
			}
		} else {
			comms.BroadcastError(event.GameId, event.Player.Id, "Config not specified")
		}
		break

	case constants.CEV_RCONFIG:
		comms.BroadcastSingle(event.GameId, event.Player.Id, comms.ServerSideEvent{
			Type: constants.SEV_CONFIG,
			Data: struct {
				Cfg map[string]string `json:"config"`
			}{config.GetAllConfig(event.GameId, db)},
		})
		break

	case constants.CEV_END_GAME:
		HostEndGame(event.GameId, event.Player.Id, db)
		break

	case constants.CEV_MOVE_ON:
		MoveOnEvent(event.GameId, event.Player.Id, db)
		break

	default:
		if !globals.IsProd() {
			log.Printf("Unknown client event type: %s", event.Type)
		}
		break
	}
}
