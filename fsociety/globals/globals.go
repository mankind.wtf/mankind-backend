package globals

const (
	SERVER_PORT    = 4998
	TIMERS_ENABLED = true
)

var inProdEnv = true

func IsProd() bool {
	return inProdEnv
}
func SetProd(prod bool) {
	inProdEnv = prod
}
