package config

import (
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"strconv"
)

var intConfigs = []string{
	constants.CFG_HAND_SIZE,
	constants.CFG_ROUND_LIMIT,
	constants.CFG_PLAYER_LIMIT,
}

// returns whether value passes its val test
func ValTest(cfgKey string, cfgVal string) bool {

	if itemInArr(cfgKey, intConfigs) { // int config
		ival, err := strconv.ParseInt(cfgVal, 10, 64)
		if err != nil {
			return false
		}
		return intTest(cfgKey, ival)
	}

	// default pass test
	return true
}

func intTest(cfgKey string, val int64) bool {

	switch cfgKey {

	case constants.CFG_HAND_SIZE:
		return val >= 3 && val <= 21

	case constants.CFG_PLAYER_LIMIT:
		return val >= 2 && val <= 1024

	case constants.CFG_ROUND_LIMIT:
		return val >= 1 && val <= 100

	default:
		return true
	}
}

func itemInArr(item string, arr []string) bool {
	for _, it := range arr {
		if it == item {
			return true
		}
	}
	return false
}
