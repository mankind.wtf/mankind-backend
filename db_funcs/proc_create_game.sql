DROP PROCEDURE IF EXISTS create_game;
DELIMITER //
CREATE PROCEDURE create_game()
BEGIN

    DECLARE the_game_id INT;

    START TRANSACTION;

    # Create blank game
    INSERT INTO game () VALUES ();
    SET the_game_id = LAST_INSERT_ID();

    # Set default config
    INSERT INTO config (game_id, cfg_key, cfg_val)
    VALUES (the_game_id, 'HAND_SIZE', 7), (the_game_id, 'PLAYER_LIMIT', 6), (the_game_id, 'ROUND_LIMIT', 5);

    COMMIT;

    # output
    SELECT the_game_id;

end//
DELIMITER ;
