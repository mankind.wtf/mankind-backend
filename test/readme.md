# FSociety testing program

Program for testing the FSociety backend.

## Command line args

    testy.go <n> [dev]

- *n* (required) - number of players to initialise. 1 minimum. 
Note that you need at least 2 players to start a game.
- *dev* (optional) - Specify "dev" if in dev mode. Anything else for prod. Mainly affects 
which server the program connects to.

## Running

Once running, the program will:

- Create a game.
- Register *n* players to the game.
- Log all players on to said game.
- Initialise a REPL where you can instruct the players to do things.

## Logging

All messages sent from and received by players are logged (with timestamp) 
to a file called `players.log` in the dir the program is run in.

## Commands

The format for REPL commands is as such:

    <player_num> <command> <args>
    
`<player_num>` is an int between 0 and *n-1* representing a local player id. 
The command and args will be sent to the local player to interpret and send
to the server. You can use `*` to send a command to all players.

At any time you can type `q` to quit the program. All players still connected
will be instructed to disconnect.

A list of commands follows:

Command | Description | Args
--- | --- | ---
`x` | Disconnect from server | n/a
`con` | Connect to server | n/a
`status` | Request full game state | n/a
`hand` | Request hand | n/a
`start` | Start the game | n/a
`play` | Play a response | space-separated ints (card ids)
`pick` | Pick a winning response | int (response id)
`wcfg` | Set game config. See main Readme | space-separated key-value pairs
`rcfg` | Get game config | n/a
`end` | End the game | n/a
`next` | Move onto next sub-round | n/a