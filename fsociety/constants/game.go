package constants

const (
	GS_LOBBY = "LOBBY"

	GS_IG_PREFIX  = "IN_GAME"
	GS_IG_PLAYING = "IN_GAME_PLAYING"
	GS_IG_PICKING = "IN_GAME_PICKING"
	GS_IG_PICKED  = "IN_GAME_WINNER_PICKED"

	GS_COMPLETED = "COMPLETED"
)

func ItemInArr(arr []int64, item int64) bool {
	for _, pid := range arr {
		if pid == item {
			return true
		}
	}
	return false
}
