package game

import (
	"database/sql"
	"fmt"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/config"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/timing"
	"log"
	"strconv"
	"strings"
)

type PlayerRanking struct {
	Nick      string `json:"nick"`
	Points    int64  `json:"points"`
	Connected bool   `json:"connected"`
	IsHost    bool   `json:"host"`
}

type QuestionCard struct {
	Text            string `json:"text"`
	AnswersRequired int    `json:"answers_required"`
}

type ResponsePlay struct {
	Id    int64    `json:"id"`
	Cards []string `json:"cards"`
}

type ResponseDetail struct {
	Count     int            `json:"count"`
	Responses []ResponsePlay `json:"responses,omitempty"`
}

type RoundPlayingData struct {
	CardCzar    string       `json:"card_czar"`
	Question    QuestionCard `json:"question"`
	RoundNum    int          `json:"round_num"`
	SubRoundNum int          `json:"sub_round_num"`
}

// IDE has issues with name "GameState"
type StateOfGame struct {
	// Always
	Status   string          `json:"game_status"`
	Rankings []PlayerRanking `json:"rankings"`

	// If IN_GAME
	InGameData     *RoundPlayingData `json:"in_game_data,omitempty"`
	ResponseDetail *ResponseDetail   `json:"response_detail,omitempty"`

	// if card czar has picked winner
	WinningResponseId int64  `json:"winning_response_id,omitempty"`
	WinningPlayer     string `json:"winning_player,omitempty"`

	// if game complete
	ResponseHistory *[]ResponseHistoryPoint `json:"response_history,omitempty"`
	HumourPrefStats *[]HumourPref           `json:"stat_humour_pref,omitempty"`
}

// Relatively expensive set of calls to get the whole game state (public view only)
// Assumes game exists
func GetGameState(gameId int64, db *sql.DB) StateOfGame {
	state := StateOfGame{
		Rankings: GetRankings(gameId, db),
		Status:   GetGameStatus(gameId, db),
	}

	if state.Status == constants.GS_LOBBY {
		return state
	} else if state.Status == constants.GS_COMPLETED {
		rh := GetResponseHistory(gameId, db)
		state.ResponseHistory = &rh
		stats := StatHumourPref(gameId, db)
		state.HumourPrefStats = &stats
		return state
	}
	// else in-game

	// get card czar, and question ===

	czar, quest := GetCzarAndQuestion(gameId, db)
	rn, srn := GetCurrentRound(gameId, db)
	state.InGameData = &RoundPlayingData{
		CardCzar:    czar.Nick,
		Question:    quest,
		RoundNum:    rn,
		SubRoundNum: srn,
	}

	// get played responses ===

	if state.Status == constants.GS_IG_PLAYING {

		// just the count
		rows, _ := db.Query(`SELECT COUNT(id) FROM response_history
            WHERE game_id = ? AND round_num = ? AND sub_round_num = ?`, gameId, rn, srn)
		rows.Next()
		var count int
		rows.Scan(&count)
		rows.Close()

		state.ResponseDetail = &ResponseDetail{Count: count}
		return state
	}
	// else czar picking or picked ===

	resps := GetRoundResponses(gameId, db)
	state.ResponseDetail = &resps

	// get winner details if necessary ===
	if state.Status == constants.GS_IG_PICKED {
		rows, _ := db.Query(`SELECT p.nick, qh.winner_id FROM question_history qh
            INNER JOIN response_history rh on qh.winner_id = rh.id
            INNER JOIN player p on rh.player_id = p.id
            WHERE qh.game_id = ?
            ORDER BY question_time DESC LIMIT 1;`, gameId)
		rows.Next()
		var (
			winningNick   string
			winningRespId int64
		)
		rows.Scan(&winningNick, &winningRespId)
		rows.Close()

		state.WinningPlayer = winningNick
		state.WinningResponseId = winningRespId
	}

	return state
}

func PlayResponse(gameId int64, playerId int64, resps []int64, db *sql.DB) {
	gStat := GetGameStatus(gameId, db)
	if gStat != constants.GS_IG_PLAYING {
		comms.BroadcastError(gameId, playerId, "Game not currently in playing phase.")
		return
	}

	quest := GetQuestionCard(gameId, db)
	if quest.AnswersRequired != len(resps) {
		comms.BroadcastError(gameId, playerId, "Incorrect number of responses given")
		return
	}

	csvResps := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(resps)), ","), "[]")
	res, _ := db.Query("CALL play_response(?, ?, ?)", gameId, playerId, csvResps)
	res.Next()
	var procStatus int
	res.Scan(&procStatus)
	res.Close()

	// some error handling
	if procStatus < 0 {
		switch procStatus {
		case -1:
			comms.BroadcastError(gameId, playerId, "Some of the cards you tried to play do not exist")
			return
		case -2:
			comms.BroadcastError(gameId, playerId, "Some of the cards you tried to play are not yours")
			return
		case -3:
			comms.BroadcastError(gameId, playerId, "You have already played this round.")
			return
		case -4:
			comms.BroadcastError(gameId, playerId, "You are the card czar, and so should not play a response.")
			return
		default:
			log.Printf("Warning! Unimplemented sql proc error code: %d", procStatus)
			return
		}
	}

	// Notify others of response ===
	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_RPLAYED,
		Data: struct{}{},
	})

	// send player new hand
	JustSendHand(gameId, playerId, db)

	if !globals.IsProd() {
		log.Printf("Player %d played response in game %d", playerId, gameId)
	}

	if procStatus == 1 { // move to picking phase
		MoveToPickingPhase(gameId, db)

	} else { // still in playing phase
		// Timer only registers after first response. So as to ensure picking phase won't start without responses.
		// Timer also only registers once, so this is effectively a round time limit starting at first response.
		if !timing.TimerIsSet(gameId, constants.TT_PLAY_TIMEOUT) {
			timing.RegisterTimer(gameId, constants.TT_PLAY_TIMEOUT, func() {
				MoveToPickingPhase(gameId, db)
			})
		}
	}
}

func KillGame(gameId int64, reason string, db *sql.DB) {
	db.Exec("UPDATE game SET status='COMPLETED' WHERE id=?", gameId)

	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_GAME_KILLED,
		Data: struct {
			Reason string `json:"reason"`
		}{reason},
	})

	timing.CancelAllTimers(gameId)

	// the state will contain updated rankings, stats and so on.
	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_STATUS,
		Data: GetGameState(gameId, db)},
	)

	if !globals.IsProd() {
		log.Printf("Game %d killed because %s", gameId, reason)
	}
}

// Switch host from one player to another in a game.
// If 1 or fewer users are connected, the game will be killed.
// Returns whether game was killed
func SwitchHost(gameId int64, oldHostId int64, db *sql.DB) bool {

	res, _ := db.Query("CALL switch_host(?, ?)", gameId, oldHostId)
	res.Next()
	var (
		shouldKillGame bool
		newHostId      sql.NullInt64
	)
	res.Scan(&shouldKillGame, &newHostId)
	res.Close()

	if newHostId.Valid {
		player := GetPlayerById(newHostId.Int64, db)
		comms.Broadcast(gameId, comms.ServerSideEvent{
			Type: constants.SEV_HOST_SWITCHED,
			Data: struct {
				Nick string `json:"nick"`
			}{player.Nick},
		})
	}

	if shouldKillGame && IsInGame(gameId, db) {
		KillGame(gameId, "Too few players connected", db)
		return true
	}

	return false
}

// Does not assign cards
func StartSubRound(gameId int64, db *sql.DB) {
	timing.CancelTimer(gameId, constants.TT_PICK_TIMEOUT)

	res, _ := db.Query("CALL start_sub_round(?)", gameId)
	defer res.Close()
	res.Next()
	var (
		czarId      int64
		newQid      int64
		newRoundNum int
		newSubRound int
	)
	res.Scan(&czarId, &newQid, &newRoundNum, &newSubRound)

	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_ROUND_START,
		Data: RoundPlayingData{GetPlayerById(czarId, db).Nick, GetQuestionCardById(newQid, db),
			newRoundNum, newSubRound},
	})

	if !globals.IsProd() {
		log.Printf("Game %d started new sub/round (%d:%d)", gameId, newRoundNum, newSubRound)
	}
}

func StartGame(gameId int64, requestingPlayerId int64, db *sql.DB) {

	if *GetHostId(gameId, db) != requestingPlayerId {
		comms.BroadcastError(gameId, requestingPlayerId, "You cannot start the game. Only the host can start the game.")
		return
	}

	if GetGameStatus(gameId, db) != constants.GS_LOBBY {
		comms.BroadcastError(gameId, requestingPlayerId, "Only games in lobby can be started.")
		return
	}

	if GetActivePlayerCount(gameId, db) < 2 {
		comms.BroadcastError(gameId, requestingPlayerId, "Not enough players to start game.")
		return
	}

	StartSubRound(gameId, db)

	AssignCardsMulti(gameId, db)

	if !globals.IsProd() {
		log.Printf("Player %d started game %d", requestingPlayerId, gameId)
	}
}

// assigns cards to every player whose hand is not full, and then sends them the update
func AssignCardsMulti(gameId int64, db *sql.DB) {

	res, _ := db.Query("CALL assign_cards_multi(?)", gameId)
	res.Next()
	var playersUpdated string
	res.Scan(&playersUpdated)
	res.Close()

	if playersUpdated == "" { // nothing updated
		return
	}

	var playersWithNewCards = strings.Split(playersUpdated, ",")
	for _, pid := range playersWithNewCards {
		parsedPid, _ := strconv.ParseInt(pid, 10, 64)
		JustSendHand(gameId, parsedPid, db)
	}

}

func JustAssignCards(gameId int64, playerId int64, numCards int, db *sql.DB) {
	db.Exec("CALL assign_cards(?, ?, ?)", gameId, playerId, numCards)
}

func JustSendHand(gameId int64, playerId int64, db *sql.DB) {
	comms.BroadcastSingle(gameId, playerId, comms.ServerSideEvent{
		Type: constants.SEV_HAND_UPDATED,
		Data: struct {
			Cards []ResponseCard `json:"cards"`
		}{GetPlayerHand(gameId, playerId, db)},
	})
}

func MoveToPickingPhase(gameId int64, db *sql.DB) {
	timing.CancelTimer(gameId, constants.TT_PLAY_TIMEOUT)

	db.Exec("UPDATE game SET status = ? WHERE id = ?", constants.GS_IG_PICKING, gameId)

	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_ROUND_PICKING,
		Data: struct {
			ResponseDetail `json:"response_detail"`
		}{GetRoundResponses(gameId, db)},
	})

	if !globals.IsProd() {
		log.Printf("Game %d moved onto picking phase", gameId)
	}

	timing.RegisterTimer(gameId, constants.TT_PICK_TIMEOUT, func() {
		if !globals.IsProd() {
			log.Printf("Skipping winner picking in game %d because of pick timeout", gameId)
		}
		PostWinnerPicking(gameId, db)
	})
}

func PostWinnerPicking(gameId int64, db *sql.DB) {
	// do checks for moving on to completion or next sub-round
	rn, srn := GetCurrentRound(gameId, db)
	rlim := config.GetConfigInt(gameId, constants.CFG_ROUND_LIMIT, db)

	if rn >= rlim && srn >= GetActivePlayerCount(gameId, db) {
		CompleteGame(gameId, db)
	} else {
		StartSubRound(gameId, db)
		AssignCardsMulti(gameId, db)
	}
}

func PickWinner(gameId int64, pickerId int64, respId int64, db *sql.DB) {

	// check that game is in-game
	if GetGameStatus(gameId, db) != constants.GS_IG_PICKING {
		comms.BroadcastError(gameId, pickerId, "Game not in picking phase.")
		return
	}

	// check that picker is czar
	res, _ := db.Query(`CALL pick_winner(?, ?, ?)`, gameId, pickerId, respId)
	res.Next()
	var (
		procStatus      int
		winningPlayerId sql.NullInt64
	)
	res.Scan(&procStatus, &winningPlayerId)
	res.Close()

	if procStatus < 0 {
		switch procStatus {
		case -1:
			comms.BroadcastError(gameId, pickerId, "Only the card czar can pick the winner")
			return
		case -2:
			comms.BroadcastError(gameId, pickerId, "Invalid response id")
			return
		default:
			log.Printf("Warning! Unimplemented sql proc status code: %d", procStatus)
			return
		}
	}

	timing.CancelTimer(gameId, constants.TT_PICK_TIMEOUT)

	// announce winner picked
	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_WINNER,
		Data: struct {
			Nick       string          `json:"winner_nick"`
			ResponseId int64           `json:"response_id"`
			Rankings   []PlayerRanking `json:"rankings"`
		}{GetPlayerById(winningPlayerId.Int64, db).Nick,
			respId, GetRankings(gameId, db)},
	})

	if !globals.IsProd() {
		log.Printf("Player %d picked winner in game %d", pickerId, gameId)
	}

	timing.RegisterTimer(gameId, constants.TT_PICKED_TO_NEW_ROUND, func() {
		PostWinnerPicking(gameId, db)
	})
}

func CompleteGame(gameId int64, db *sql.DB) {

	db.Exec("UPDATE game SET status = ? WHERE id=?", constants.GS_COMPLETED, gameId)

	timing.CancelAllTimers(gameId) // just in case

	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_COMPL,
		Data: struct{}{},
	})

	// the state will contain updated rankings, stats and so on.
	comms.Broadcast(gameId, comms.ServerSideEvent{
		Type: constants.SEV_STATUS,
		Data: GetGameState(gameId, db)},
	)

	if !globals.IsProd() {
		log.Printf("Game %d complete", gameId)
	}

}

func HostEndGame(gameId int64, requestorId int64, db *sql.DB) {
	if requestorId != *GetHostId(gameId, db) {
		comms.BroadcastError(gameId, requestorId, "Only the host can force end a game")
		return
	}

	if !IsInGame(gameId, db) {
		comms.BroadcastError(gameId, requestorId, "Game is not in session")
		return
	}

	KillGame(gameId, "Host ended game", db)
}

func MoveOnEvent(gameId int64, requestorId int64, db *sql.DB) {
	if requestorId != *GetHostId(gameId, db) {
		comms.BroadcastError(gameId, requestorId, "Only the host can move the game to the next phase")
		return
	}

	if !IsInGame(gameId, db) {
		comms.BroadcastError(gameId, requestorId, "Game is not in session")
		return
	}

	if !globals.IsProd() {
		log.Printf("Game %d forced to next game phase by host", gameId)
	}

	switch GetGameStatus(gameId, db) {
	case constants.GS_IG_PLAYING:
		if GetRoundResponses(gameId, db).Count == 0 {
			StartSubRound(gameId, db)
		} else {
			MoveToPickingPhase(gameId, db)
		}

	case constants.GS_IG_PICKING, constants.GS_IG_PICKED:

		rn, srn := GetCurrentRound(gameId, db)
		rlim := config.GetConfigInt(gameId, constants.CFG_ROUND_LIMIT, db)

		if rn >= rlim && srn >= GetActivePlayerCount(gameId, db) {
			// move to completion
			CompleteGame(gameId, db)
		} else {
			StartSubRound(gameId, db)
			AssignCardsMulti(gameId, db)
		}
	}
}
