package config

import (
	"database/sql"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"strconv"
)

var configHub = make(map[int64]map[string]string)

// Returns true if config successfully updated. False if at least one kv-pair has invalid values
//   (no config updated in this case).
func SetAllConfig(gameId int64, cfg map[string]string, db *sql.DB) bool {
	for k, v := range cfg {
		if !ValTest(k, v) || !itemInArr(k, constants.ALL_CFG) {
			return false
		}
	}

	for k, v := range cfg {
		setConfig(gameId, k, v, db)
	}
	return true
}

func setConfig(gameId int64, cfgKey string, cfgVal string, db *sql.DB) {
	cfg, ok := configHub[gameId]
	if !ok {
		configHub[gameId] = make(map[string]string)
		cfg = configHub[gameId]
	}

	cfg[cfgKey] = cfgVal

	db.Exec("UPDATE config SET cfg_val=? WHERE cfg_key=? AND game_id=?", cfgVal, cfgKey, gameId)
}

// read config from db
func readInConfig(gameId int64, db *sql.DB) {

	cfgPlatter, ok := configHub[gameId]
	if !ok {
		configHub[gameId] = make(map[string]string)
		cfgPlatter = configHub[gameId]
	}

	res, _ := db.Query("SELECT cfg_key, cfg_val FROM config WHERE game_id = ?", gameId)
	defer res.Close()

	for res.Next() {
		var (
			ck string
			cv string
		)
		res.Scan(&ck, &cv)
		cfgPlatter[ck] = cv
	}
}

// Reads from program memory - Not expecting external updates to config
func GetConfigInt(gameId int64, cfgKey string, db *sql.DB) int {
	if _, ok := configHub[gameId]; !ok {
		readInConfig(gameId, db)
	}
	vstr := configHub[gameId][cfgKey]
	i, _ := strconv.ParseInt(vstr, 10, 32)
	return int(i)
}

func GetAllConfig(gameId int64, db *sql.DB) map[string]string {
	if _, ok := configHub[gameId]; !ok {
		readInConfig(gameId, db)
	}
	return configHub[gameId]
}
