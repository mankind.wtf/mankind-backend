package routes

import (
	"net/http"
)

type RouteDef struct {
	Path    string
	Handler func(http.ResponseWriter, *http.Request)
}

func GetRouteHandlers() []RouteDef {
	return []RouteDef{
		{"/game", gameHandler},
		{"/game/{game_id}", registerUserToGame},
		{"/gamestate/{game_id}", gameStateHandler},
		{"/suggestion", suggestCard},
	}
}
