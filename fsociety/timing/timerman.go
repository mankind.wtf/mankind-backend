package timing

import (
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"time"
)

type CustomTimer struct {
	Timer      *time.Timer
	CancelChan chan bool
	Fired      bool
}

func (timer CustomTimer) Cancel() {
	if timer.Timer.Stop() { // hasn't fired
		timer.CancelChan <- true
	}
}

var timerHub = make(map[int64]map[string]*CustomTimer)

// Registers a function to execute on a game in the specified amount of time
// If that type of timer is already set, it is cancelled and overwritten
func RegisterTimer(gameId int64, timerType string, fun func()) {

	if !globals.TIMERS_ENABLED {
		return
	}

	timerTypeMap, ok := timerHub[gameId]
	if !ok {
		timerHub[gameId] = make(map[string]*CustomTimer)
		timerTypeMap = timerHub[gameId]
	}

	if timer, ok := timerTypeMap[timerType]; ok {
		timer.Cancel()
	}

	newTimer := &CustomTimer{
		Timer:      time.NewTimer(constants.TIMER_DURATIONS[timerType]),
		CancelChan: make(chan bool),
		Fired:      false,
	}
	timerTypeMap[timerType] = newTimer

	// Spin off an immediately-blocking func waiting for either the call or the cancellation
	go func() {
		select {
		case <-newTimer.Timer.C:
			fun()
			break
		case <-newTimer.CancelChan:
			break
		}
		newTimer.Fired = true
	}()
}

// Returns true if timer is registered and hasn't fired.
// False if not registered or already fired.
func TimerIsSet(gameId int64, timerType string) bool {
	if timerTypeMap, ok := timerHub[gameId]; ok {
		if timer, ok := timerTypeMap[timerType]; ok {
			return !timer.Fired
		}
	}
	return false
}

func CancelTimer(gameId int64, timerType string) {
	if timerTypeMap, ok := timerHub[gameId]; ok {
		if timer, ok := timerTypeMap[timerType]; ok {
			timer.Cancel()
		}
	}
}

// cancels all timers set for a game
func CancelAllTimers(gameId int64) {
	if timerTypeMap, ok := timerHub[gameId]; ok {
		for _, timer := range timerTypeMap {
			timer.Cancel()
		}
	}
}
