package game

import (
	"database/sql"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/config"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"log"
	"math/rand"
	"strings"
)

type ResponseCard struct {
	CardId   int64  `json:"card_id"`
	CardText string `json:"card_text"`
}

type ResponseHistoryPoint struct {
	Question  string   `json:"question"`
	Responses []string `json:"responses"`
	CardCzar  string   `json:"card_czar"`
	Winner    string   `json:"winner"`
}

func CreateGame(nick string, db *sql.DB) (int64, Player) {
	res, _ := db.Query("CALL create_game()")
	res.Next()
	var gameId int64
	res.Scan(&gameId)
	res.Close()

	// Only possible error is 409, and that cannot happen on game create
	player, _ := CreatePlayer(nick, gameId, db)

	return gameId, *player
}

func GameExists(gameId int64, db *sql.DB) bool {
	res, _ := db.Query("SELECT id, status FROM game WHERE id=?", gameId)
	defer res.Close()
	return res.Next()
}

func IsInGame(gameId int64, db *sql.DB) bool {
	return strings.HasPrefix(GetGameStatus(gameId, db), constants.GS_IG_PREFIX)
}

func GetActivePlayerCount(gameId int64, db *sql.DB) int {
	res, _ := db.Query("SELECT COUNT(player_id) FROM game_players WHERE game_id = ? AND is_connected = TRUE", gameId)
	defer res.Close()
	res.Next()
	var (
		count int
	)
	res.Scan(&count)
	return count
}

func GetGameStatus(gameId int64, db *sql.DB) string {
	res, _ := db.Query("SELECT status FROM game WHERE id=?", gameId)
	defer res.Close()
	res.Next()
	var stat string
	res.Scan(&stat)
	return stat
}

func GetQuestionCardById(qId int64, db *sql.DB) QuestionCard {
	res, _ := db.Query("SELECT question, responses_required FROM question_card WHERE id = ?", qId)
	defer res.Close()
	res.Next()
	var (
		text    string
		respReq int
	)
	res.Scan(&text, &respReq)
	return QuestionCard{text, respReq}
}

func GetQuestionCard(gameId int64, db *sql.DB) QuestionCard {
	res, _ := db.Query(`SELECT qc.question, qc.responses_required FROM question_history qh 
    			INNER JOIN question_card qc on qh.question_id = qc.id
				WHERE qh.game_id = ? 
				ORDER BY qh.question_time DESC LIMIT 1`, gameId)
	defer res.Close()
	res.Next()
	var (
		text    string
		respReq int
	)
	res.Scan(&text, &respReq)
	return QuestionCard{text, respReq}
}

func GetCurrentRound(gameId int64, db *sql.DB) (int, int) {
	res, _ := db.Query(`SELECT round_num, sub_round_num FROM question_history
		WHERE game_id = ?
		ORDER BY round_num DESC, sub_round_num DESC LIMIT 1;`, gameId)
	defer res.Close()
	res.Next()
	var (
		rn  int
		srn int
	)
	res.Scan(&rn, &srn)
	return rn, srn
}

func GetRoundResponses(gameId int64, db *sql.DB) ResponseDetail {
	rn, srn := GetCurrentRound(gameId, db)

	rows, _ := db.Query(`SELECT rh.id, rc.response FROM response_history rh
        INNER JOIN response_history_cards rhc on rh.id = rhc.response_id
        INNER JOIN response_card rc on rhc.response_card_id = rc.id
        WHERE rh.game_id = ?
        AND rh.round_num = ? AND rh.sub_round_num = ?
        ORDER BY rh.id, rhc.ordering`, gameId, rn, srn)
	defer rows.Close()

	resps := make([]ResponsePlay, 0)
	resp := ResponsePlay{Cards: make([]string, 0)}
	for rows.Next() { // handles any-n card responses
		var (
			id       int64
			respText string
		)
		rows.Scan(&id, &respText)

		if resp.Id != 0 && resp.Id != id {
			resps = append(resps, resp)
			resp = ResponsePlay{Cards: make([]string, 0)}
		}
		resp.Id = id
		resp.Cards = append(resp.Cards, respText)
	}
	if len(resp.Cards) > 0 { // only add if there was at least one response
		resps = append(resps, resp)

		rand.Seed(gameId * int64(rn) * int64(srn)) // shuffle the responses the same way for everyone in the game,
		// with a new shuffle each sub-round
		rand.Shuffle(len(resps), func(i, j int) { resps[i], resps[j] = resps[j], resps[i] })
	}

	return ResponseDetail{
		Count:     len(resps),
		Responses: resps,
	}
}

func GetCzarAndQuestion(gameId int64, db *sql.DB) (Player, QuestionCard) {
	rows, _ := db.Query(`SELECT p.nick, p.id, qc.question, qc.responses_required FROM question_history qh
        INNER JOIN question_card qc on qh.question_id = qc.id
        INNER JOIN player p on qh.card_czar_id = p.id
        WHERE qh.game_id = ?
        ORDER BY qh.round_num DESC, qh.sub_round_num DESC LIMIT 1`, gameId)
	rows.Next()
	var (
		czNick  string
		czId    int64
		quest   string
		respReq int
	)
	rows.Scan(&czNick, &czId, &quest, &respReq)
	rows.Close()

	return Player{Nick: czNick, Id: czId}, QuestionCard{Text: quest, AnswersRequired: respReq}
}

func GetRankings(gameId int64, db *sql.DB) []PlayerRanking {
	rows, _ := db.Query(`SELECT gp.points, p.nick, gp.is_connected, gp.is_host FROM game_players gp  
        INNER JOIN player p on gp.player_id = p.id
        WHERE game_id = ? ORDER BY gp.points DESC`, gameId)
	defer rows.Close()

	ranks := make([]PlayerRanking, 0)
	for rows.Next() {
		var (
			points    int64
			nick      string
			connected bool
			host      bool
		)
		rows.Scan(&points, &nick, &connected, &host)
		ranks = append(ranks, PlayerRanking{nick, points, connected, host})
	}

	return ranks
}

func GetPlayerHand(gameId int64, playerId int64, db *sql.DB) []ResponseCard {
	res, _ := db.Query(`SELECT rc.id, rc.response FROM hands h
		INNER JOIN response_card rc on h.response_card_id = rc.id
		WHERE h.game_id=? AND h.player_id = ?
		ORDER BY rc.id`, gameId, playerId)
	defer res.Close()

	resps := make([]ResponseCard, 0)
	for res.Next() {
		var (
			respId   int64
			respText string
		)
		res.Scan(&respId, &respText)
		resps = append(resps, ResponseCard{respId, respText})
	}

	return resps
}

// Gets number of players registered to game (whether connected or not)
func GetNumPlayers(gameId int64, db *sql.DB) int {
	res, _ := db.Query("SELECT COUNT(player_id) FROM game_players WHERE game_id = ?", gameId)
	defer res.Close()
	res.Next()
	var count int
	res.Scan(&count)
	return count
}

func ConfigUpdateEvent(gameId int64, requestorId int64, cfg map[string]string, db *sql.DB) {

	host := *GetHostId(gameId, db)

	if requestorId != host {
		comms.BroadcastError(gameId, requestorId, "Only the host can alter game config.")
		return
	}

	if GetGameStatus(gameId, db) != constants.GS_LOBBY {
		comms.BroadcastError(gameId, requestorId, "Config can only be changed in lobby.")
		return
	}

	if config.SetAllConfig(gameId, cfg, db) {

		comms.Broadcast(gameId, comms.ServerSideEvent{
			Type: constants.SEV_CONFIG,
			Data: struct {
				Config map[string]string `json:"config"`
			}{config.GetAllConfig(gameId, db)},
		})

		if !globals.IsProd() {
			log.Printf("Config updated for game %d", gameId)
		}

	} else {
		comms.BroadcastError(gameId, requestorId, "Specified config is invalid")
	}
}

// returns whether everyone connected to the game (excl czar) has played a response
func EveryonePlayed(gameId int64, db *sql.DB) bool {

	res, _ := db.Query("SELECT everyone_played(?)", gameId)
	defer res.Close()
	res.Next()
	var played bool
	res.Scan(&played)
	return played
}

func GetResponseHistory(gameId int64, db *sql.DB) []ResponseHistoryPoint {

	rows, _ := db.Query(`SELECT qc.question, rc.response, pc.nick AS czar, pw.nick AS winner FROM question_history qh
		INNER JOIN question_card qc on qh.question_id = qc.id
		INNER JOIN player pc on qh.card_czar_id = pc.id
		INNER JOIN response_history rh on qh.winner_id = rh.id
		INNER JOIN response_history_cards rhc on rh.id = rhc.response_id
		INNER JOIN response_card rc on rhc.response_card_id = rc.id
		INNER JOIN player pw on rh.player_id = pw.id
		WHERE qh.game_id = ?
		ORDER BY qh.question_time ASC, rhc.ordering`, gameId)
	defer rows.Close()

	history := make([]ResponseHistoryPoint, 0)
	point := ResponseHistoryPoint{Responses: make([]string, 0)}
	var (
		question string
		response string
		czar     string
		winner   string
	)
	for rows.Next() { // handles any-n card responses

		rows.Scan(&question, &response, &czar, &winner)

		// if new round
		if point.Question != "" && point.Question != question {
			history = append(history, point)
			point = ResponseHistoryPoint{Responses: make([]string, 0)}
		}

		// pack in data
		point.Question = question
		point.CardCzar = czar
		point.Winner = winner
		point.Responses = append(point.Responses, response)
	}

	if point.Question != "" { // only add if there was at least one response
		// do last packing, after last line
		history = append(history, point)
	}

	return history
}

type HumourPref struct {
	CardCzar   string `json:"card_czar"`
	Responder  string `json:"responder"`
	PercPicked int    `json:"perc_picked"`
}

func StatHumourPref(gameId int64, db *sql.DB) []HumourPref {
	res, _ := db.Query("CALL stat_humour_pref(?)", gameId)
	defer res.Close()

	prefs := make([]HumourPref, 0)
	for res.Next() {
		var (
			czar      string
			responder string
			perc      int
		)
		res.Scan(&czar, &responder, &perc)
		prefs = append(prefs, HumourPref{czar, responder, perc})
	}

	return prefs
}
