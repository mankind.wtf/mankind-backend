# This step must happen in the buildx context
FROM golang:1.17-alpine AS builder
WORKDIR /src
COPY . .
RUN go get -v && go build -o ./fsocietyback

FROM alpine
WORKDIR /app
COPY --from=builder /src/fsocietyback .
CMD /app/fsocietyback
