DROP PROCEDURE IF EXISTS assign_cards_multi;
DELIMITER //
CREATE PROCEDURE assign_cards_multi(the_game_id INT)
BEGIN

    DECLARE default_hand_size INT;
    DECLARE tmp_cards_in_hand INT;
    DECLARE tmp_player_id INT;
    DECLARE players_updated VARCHAR(255) DEFAULT '';
    DECLARE done_looping BOOLEAN DEFAULT FALSE;

    DECLARE players_cursor CURSOR FOR
        SELECT player_id FROM game_players WHERE game_id = the_game_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done_looping = TRUE;

    SELECT cfg_val INTO default_hand_size FROM config
        WHERE game_id = the_game_id AND cfg_key = 'HAND_SIZE';

    START TRANSACTION;

    OPEN players_cursor;
    card_assign_loop: LOOP

        FETCH players_cursor INTO tmp_player_id;

        SELECT COUNT(response_card_id) INTO tmp_cards_in_hand
        FROM hands WHERE game_id = the_game_id AND player_id = tmp_player_id;

        IF tmp_cards_in_hand < default_hand_size THEN
            CALL assign_cards(the_game_id, tmp_player_id,
                default_hand_size-tmp_cards_in_hand);

            IF CHAR_LENGTH(players_updated) = 0 THEN
                SET players_updated = tmp_player_id;
            ELSE
                SET players_updated = CONCAT(players_updated, ',', tmp_player_id);
            end if;
        END IF;

        IF done_looping THEN
            LEAVE card_assign_loop;
        end if;

    end loop;
    CLOSE players_cursor;

    COMMIT;

    # output
    SELECT players_updated;

end//
DELIMITER ;