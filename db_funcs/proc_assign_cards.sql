DROP PROCEDURE IF EXISTS assign_cards;
DELIMITER //
CREATE PROCEDURE assign_cards(the_game_id INT, the_player_id INT, num_cards INT)
BEGIN

    START TRANSACTION;

    INSERT INTO hands (game_id, player_id, response_card_id)
    SELECT the_game_id, the_player_id, excl_history.id FROM

        ( # exclude cards that have been played before
            SELECT rc.id
            FROM response_card rc
                     LEFT JOIN
                 (SELECT response_card_id AS id
                  FROM response_history_cards rhc
                           INNER JOIN response_history rh on rhc.response_id = rh.id
                  WHERE rh.game_id = the_game_id) AS past_played ON rc.id = past_played.id
            WHERE past_played.id IS NULL
        ) AS excl_history

            # exclude cards that are in hands
            LEFT JOIN (SELECT response_card_id AS id FROM hands WHERE game_id=the_game_id) AS in_hands
                      ON in_hands.id = excl_history.id
            WHERE in_hands.id IS NULL

    # random and limit
    ORDER BY RAND() LIMIT num_cards;

    COMMIT;

end//
DELIMITER ;