package routes

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/config"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/constants"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/game"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"log"
	"net/http"
	"strconv"
	"sync"
)

var (
	regMu sync.Mutex
)

func gameHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var req struct {
			Nick string `json:"nick"`
		}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			comms.WriteFailure(w, 400, err.Error())
			return
		}
		if req.Nick == "" {
			comms.WriteFailure(w, 400, "Field 'nick' undefined.")
			return
		}

		db := fsociety.GetDB()
		gameId, player := game.CreateGame(req.Nick, db)

		comms.WriteJson(w, struct {
			GameId     int64  `json:"game_id"`
			PlayerAuth string `json:"player_auth"`
			Success    bool   `json:"success"`
		}{gameId, player.Auth, true}, 201)

		if !globals.IsProd() {
			log.Printf("Player %d created game %d.", player.Id, gameId)
		}

	} else {
		comms.WriteFailure405(w)
	}
}

func registerUserToGame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		gameId, err := strconv.ParseInt(mux.Vars(r)["game_id"], 10, 64)
		if err != nil {
			comms.WriteFailure(w, 400, "Cannot parse gameId")
			return
		}

		var req struct {
			Nick string `json:"nick"`
		}
		err = json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			comms.WriteFailure(w, 400, err.Error())
			return
		}
		if req.Nick == "" {
			comms.WriteFailure(w, 400, "Field 'nick' undefined.")
			return
		}

		// one user registration at a time, please
		regMu.Lock()
		defer regMu.Unlock()

		db := fsociety.GetDB()

		// check that game exists
		if !game.GameExists(gameId, db) {
			comms.WriteFailure(w, 404, "Game does not exist")
			return
		}

		if game.GetGameStatus(gameId, db) == constants.GS_COMPLETED {
			comms.WriteFailure(w, 503, "The game is completed")
			return
		}

		// check that player limit not reached
		if game.GetNumPlayers(gameId, db) >= config.GetConfigInt(gameId, constants.CFG_PLAYER_LIMIT, db) {
			comms.WriteFailure(w, 503, "The game's player limit has been reached.")
			return
		}

		player, fail := game.CreatePlayer(req.Nick, gameId, db)
		if fail != nil {
			comms.WriteFail(w, *fail)
			return
		}

		// assign player cards if game in-game
		if game.IsInGame(gameId, db) {
			game.JustAssignCards(gameId, player.Id, config.GetConfigInt(gameId, constants.CFG_HAND_SIZE, db), db)
		}

		comms.WriteJson(w, struct {
			PlayerAuth string `json:"player_auth"`
			Success    bool   `json:"success"`
		}{player.Auth, true}, 201)

		if !globals.IsProd() {
			log.Printf("Registered player %d to game %d", player.Id, gameId)
		}

	} else {
		comms.WriteFailure405(w)
	}
}
