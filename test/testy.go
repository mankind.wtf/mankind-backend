package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	HTTP_DEV  = "http://localhost:4998"
	HTTP_PROD = "https://fsocietyback.athena.rauten.co.za"
	WS_DEV    = "ws://localhost:4998"
	WS_PROD   = "wss://fsocietyback.athena.rauten.co.za"
)

var inProd = true
var hub []chan []string
var running []bool
var auths []string

func runPlayer(playerId int, gameId int64) {
	playerName := fmt.Sprintf("Test Player %d", playerId)

	if auths[playerId] == "" {
		// register player
		resp, err := http.Post(fmt.Sprintf("%s/game/%d", getUrl(false), gameId), "application/json",
			bytes.NewBuffer([]byte(fmt.Sprintf("{\"nick\":\"%s\"}", playerName))))
		errFatal(err)

		b, err := ioutil.ReadAll(resp.Body)
		errFatal(err)
		var data struct {
			PlayerAuth string `json:"player_auth"`
		}
		err = json.Unmarshal(b, &data)
		errFatal(err)
		auths[playerId] = data.PlayerAuth
	}

	// log on to game
	con, _, err := websocket.DefaultDialer.Dial(
		fmt.Sprintf("%s/gamestate/%d?auth=%s", getUrl(true), gameId, auths[playerId]), nil)
	errFatal(err)
	log.Printf("%s connected", playerName)
	defer log.Printf("%s disconnected", playerName)
	defer func() {
		running[playerId] = false
		con.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		time.Sleep(1 * time.Second)
		con.Close()
	}()

	messageReceiver := make(chan string)
	errChan := make(chan error)

	go func() {
		for {
			_, b, err := con.ReadMessage()
			if err != nil {
				errChan <- err
				break
			}
			messageReceiver <- string(b)
		}
	}()

	for {
		select {
		case msg := <-messageReceiver:
			log.Printf("%s <- %s", playerName, msg)

		case err := <-errChan:
			log.Printf("%s ERROR: %s", playerName, err.Error())

		case cmd := <-hub[playerId]:
			switch cmd[0] {
			case "x":
				return // trigger deferred disconnect funcs

			case "status":
				go sendMessage(con, playerName, "STATUS_REQ", struct{}{})
				break

			case "hand":
				go sendMessage(con, playerName, "HAND_REQ", struct{}{})
				break

			case "start":
				go sendMessage(con, playerName, "START_GAME", struct{}{})
				break

			case "play":
				cards := make([]int64, len(cmd)-1)
				for i, cstring := range cmd[1:] {
					cid, _ := strconv.ParseInt(cstring, 10, 64)
					cards[i] = cid
				}
				go sendMessage(con, playerName, "PLAY_RESPONSE", struct {
					Cards []int64 `json:"card_ids"`
				}{cards})
				break

			case "pick":
				respId, _ := strconv.ParseInt(cmd[1], 10, 64)
				go sendMessage(con, playerName, "PICK_WINNER", struct {
					RespId int64 `json:"response_id"`
				}{RespId: respId})
				break

			case "wcfg":
				cfg := make(map[string]string)
				for i := 1; i < len(cmd); i += 2 {
					cfg[cmd[i]] = cmd[i+1]
				}
				go sendMessage(con, playerName, "SET_CONFIG", struct {
					Cfg map[string]string `json:"config"`
				}{cfg})
				break

			case "rcfg":
				go sendMessage(con, playerName, "GET_CONFIG", struct{}{})
				break

			case "end":
				go sendMessage(con, playerName, "END_GAME", struct{}{})
				break

			case "next":
				go sendMessage(con, playerName, "MOVE_ON", struct{}{})
				break

			default:
				println("Unknown command:", cmd[0])
				break
			}
		}
	}
}

func sendMessage(con *websocket.Conn, playerName string, tipe string, data interface{}) {
	b, _ := json.Marshal(struct {
		Type string      `json:"type"`
		Data interface{} `json:"data"`
	}{tipe, data})
	log.Printf("%s -> %s", playerName, b)
	err := con.WriteMessage(websocket.TextMessage, b)
	errFatal(err)
}

func getUrl(ws bool) string { // because go is too cool to support the elvis operator :/
	if ws {
		if inProd {
			return WS_PROD
		} else {
			return WS_DEV
		}
	} else {
		if inProd {
			return HTTP_PROD
		} else {
			return HTTP_DEV
		}
	}
}

func main() {
	if len(os.Args) < 2 {
		errFatal(errors.New("player count not specified"))
	}

	playerCount, err := strconv.ParseInt(os.Args[1], 10, 32)
	errFatal(err)

	if playerCount < 1 {
		errFatal(errors.New("need at least 1 player"))
	}

	if len(os.Args) >= 3 && os.Args[2] == "dev" {
		inProd = false
		println("Using dev server")
	} else {
		println("Using prod server")
	}

	// create game
	resp, err := http.Post(fmt.Sprintf("%s/game", getUrl(false)), "application/json",
		bytes.NewBuffer([]byte("{\"nick\":\"Test Player 0\"}")))
	errFatal(err)

	b, err := ioutil.ReadAll(resp.Body)
	errFatal(err)
	var data struct {
		PlayerAuth string `json:"player_auth"`
		Success    bool   `json:"success"`
		GameId     int64  `json:"game_id"`
	}
	err = json.Unmarshal(b, &data)
	errFatal(err)

	println("Game", data.GameId, "created")

	// setup player logging
	f, err := os.OpenFile("players.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	errFatal(err)
	defer f.Close()
	log.SetOutput(f)
	log.Println("========")

	// spin off players
	println("Registering", playerCount, "players")

	hub = make([]chan []string, playerCount)
	running = make([]bool, playerCount)
	auths = make([]string, playerCount)
	for i := 0; i < int(playerCount); i++ {
		hub[i] = make(chan []string)
		running[i] = true
	}
	auths[0] = data.PlayerAuth
	for i := 0; i < int(playerCount); i++ {
		go runPlayer(i, data.GameId)
	}

	println("WS messages are being logged to 'players.log'")

	// start REPL
	stdin := bufio.NewReader(os.Stdin)
	for {
		print("> ")
		inp, _ := stdin.ReadString('\n')
		inp = strings.TrimSpace(inp)

		if inp == "q" {
			for i, ch := range hub {
				if running[i] {
					ch <- []string{"x"}
				}
			}
			break
		}

		splat := strings.Split(inp, " ")

		if len(splat) < 2 {
			println("Too little information to be a command")
			continue
		}

		if splat[0] == "*" {
			for i, ch := range hub {
				if splat[1] == "con" {
					reconnectPlayer(i, data.GameId)
				} else if running[i] {
					ch <- splat[1:]
				}
			}
			continue
		}

		playerId, err := strconv.ParseInt(splat[0], 10, 32)
		if err != nil || (playerId < 0) || (playerId >= playerCount) {
			println("Player", playerId, "not found")
			continue
		}

		if splat[1] == "con" {
			reconnectPlayer(int(playerId), data.GameId)
			continue
		}

		if splat[1] == "x" && !running[playerId] {
			println("Player", playerId, "already disconnected")
			continue
		}

		if !running[playerId] {
			println("Player", playerId, "no longer connected")
			continue
		}

		hub[playerId] <- splat[1:]
	}

	// give some disconnect time
	time.Sleep(1 * time.Second)
}

func reconnectPlayer(playerId int, gameId int64) {
	if running[playerId] {
		println("Player", playerId, "already connected")
		return
	}

	hub[playerId] = make(chan []string)
	running[playerId] = true
	go runPlayer(playerId, gameId)
}

func errFatal(err error) {
	if err != nil {
		fmt.Println("ERROR:")
		fmt.Println(err)
		os.Exit(1)
	}
}
