package game

import (
	"crypto/md5"
	"database/sql"
	"fmt"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/comms"
	"math/rand"
	"time"
)

type Player struct {
	Id   int64
	Auth string
	Nick string
}

// Create a player for a game and return their details.
// Note: does not check if game exists
func CreatePlayer(nick string, gameId int64, db *sql.DB) (*Player, *comms.FailureResponse) {

	res, _ := db.Query("SELECT p.id FROM game_players gp INNER JOIN player p on gp.player_id = p.id WHERE gp.game_id=? AND p.nick=?",
		gameId, nick)
	if res.Next() {
		return nil, &comms.FailureResponse{Code: 409,
			Message: fmt.Sprintf("Player with nick '%s' already registered for game %d", nick, gameId)}
	}
	res.Close()

	authb := md5.Sum([]byte(fmt.Sprintf("%s-%x-%f", nick, time.Now().UnixNano(), rand.Float32())))
	auth := fmt.Sprintf("%x", authb)

	execres, _ := db.Exec("INSERT INTO player (nick, auth) VALUES (?,?)", nick, auth)
	playerId, _ := execres.LastInsertId()

	// insert into game
	db.Exec("INSERT INTO game_players (game_id, player_id) VALUES (?, ?)", gameId, playerId)

	return &Player{playerId, auth, nick}, nil
}

// Check player by auth. Also checks that player is part of game
func CheckPlayerAuth(auth string, gameId int64, db *sql.DB) (*Player, *comms.FailureResponse) {
	res, _ := db.Query("SELECT id, nick FROM player WHERE auth=?", auth)
	if !res.Next() {
		return nil, &comms.FailureResponse{Code: 401, Message: "Invalid Authorization"}
	}

	var (
		id   int64
		nick string
	)
	res.Scan(&id, &nick)
	res.Close()
	playa := Player{id, auth, nick}

	// check that player part of game
	res, _ = db.Query("SELECT player_id FROM game_players WHERE game_id=? AND player_id=?", gameId, playa.Id)
	if !res.Next() {
		return nil, &comms.FailureResponse{Code: 403, Message: "You are not registered to this game."}
	}
	res.Close()

	return &playa, nil
}

func GetPlayerById(playerId int64, db *sql.DB) Player {
	res, _ := db.Query("SELECT nick, auth FROM player WHERE id = ?", playerId)
	defer res.Close()
	res.Next()
	var (
		nick string
		auth string
	)
	res.Scan(&nick, &auth)
	return Player{playerId, auth, nick}
}

// Get game's host id. Nil if no host set yet.
// Assumes game exists.
func GetHostId(gameId int64, db *sql.DB) *int64 {
	res, _ := db.Query("SELECT player_id FROM game_players WHERE game_id = ? AND is_host=TRUE", gameId)
	defer res.Close()
	if !res.Next() {
		return nil
	}
	var hid int64
	res.Scan(&hid)
	return &hid
}
