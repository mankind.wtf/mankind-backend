DROP FUNCTION IF EXISTS everyone_played;
DELIMITER //
CREATE FUNCTION everyone_played(the_game_id INT) RETURNS BOOLEAN
READS SQL DATA
DETERMINISTIC
BEGIN

    DECLARE cz_id INT;
    DECLARE responses_played INT;
    DECLARE active_player_count INT;
    DECLARE rn INT;
    DECLARE srn INT;

    SELECT card_czar_id, round_num, sub_round_num INTO cz_id, rn, srn
    FROM question_history
    WHERE question_history.game_id = the_game_id
    ORDER BY question_time DESC
    LIMIT 1;

    SELECT COUNT(player_id) INTO active_player_count
    FROM game_players
    WHERE game_id = the_game_id
      AND is_connected = TRUE
      AND player_id != cz_id;

    SELECT COUNT(rh.player_id) INTO responses_played FROM response_history rh
        INNER JOIN
        (
            SELECT player_id
            FROM game_players
            WHERE game_id = the_game_id
              AND is_connected = TRUE
              AND player_id != cz_id
        ) AS active_players
        ON rh.player_id = active_players.player_id
    WHERE rh.game_id = the_game_id AND rh.round_num = rn AND rh.sub_round_num = srn;

    RETURN responses_played = active_player_count;

end//
DELIMITER ;
