package comms

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
)

type FailureResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message,omitempty"`
}

// REST senders ===

func WriteJson(w http.ResponseWriter, o interface{}, code int) {
	w.Header().Add("Content-Type", "application/json")
	addCorsHeaders(w)
	w.WriteHeader(code)
	b, _ := json.Marshal(o)
	fmt.Fprint(w, string(b))
}

func WriteFail(w http.ResponseWriter, response FailureResponse) {
	w.Header().Add("Content-Type", "application/json")
	addCorsHeaders(w)
	w.WriteHeader(response.Code)
	b, _ := json.Marshal(response)
	fmt.Fprint(w, string(b))
}

func WriteFailure(w http.ResponseWriter, code int, message string) {
	WriteFail(w, FailureResponse{code, message})
}

func WriteFailure405(w http.ResponseWriter) {
	WriteFailure(w, 405, "The requested method has no functionality at this endpoint.")
}

// REST helpers

func addCorsHeaders(w http.ResponseWriter) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Request-Method", "POST")
}

// WS helpers

type ServerSideEvent struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

func SendMessage(con *websocket.Conn, mtype string, data interface{}) error {
	blob := ServerSideEvent{mtype, data}
	allBytes, _ := json.Marshal(blob)
	return con.WriteMessage(websocket.TextMessage, allBytes)
}

func SendMsg(con *websocket.Conn, event ServerSideEvent) error {
	allBytes, _ := json.Marshal(event)
	return con.WriteMessage(websocket.TextMessage, allBytes)
}
