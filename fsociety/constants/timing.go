package constants

import "time"

const ( // timer types
	TT_PICKED_TO_NEW_ROUND = "PICKED_TO_NEW_ROUND"
	TT_PLAY_TIMEOUT        = "PLAYING_TO_PICKING"
	TT_PICK_TIMEOUT        = "PICKING_TO_PLAYING"
)

var TIMER_DURATIONS = map[string]time.Duration{
	TT_PICKED_TO_NEW_ROUND: 5 * time.Second,
	TT_PLAY_TIMEOUT:        1*time.Minute + 30*time.Second,
	TT_PICK_TIMEOUT:        1*time.Minute + 30*time.Second,
}
