DROP PROCEDURE IF EXISTS start_sub_round;
DELIMITER //
CREATE PROCEDURE start_sub_round(the_game_id INT)
BEGIN

    DECLARE cur_round_num INT;
    DECLARE cur_sub_round_num INT;
    DECLARE new_question_id INT;
    DECLARE new_round_num INT;
    DECLARE new_sub_round_num INT;
    DECLARE num_candidates INT;
    DECLARE czar_id INT;

    START TRANSACTION;

    # update game status
    UPDATE game SET status = 'IN_GAME_PLAYING' WHERE id = the_game_id;

    # pick new question card
    SELECT qc.id INTO new_question_id FROM question_card qc
        LEFT JOIN (SELECT qh.question_id AS id FROM question_history qh WHERE game_id=the_game_id) AS past_questions
        on qc.id = past_questions.id
    WHERE past_questions.id IS NULL
    ORDER BY RAND() LIMIT 1;

    # get round
    SELECT COUNT(DISTINCT round_num) INTO cur_round_num FROM question_history WHERE game_id = the_game_id;

    IF cur_round_num = 0 THEN # new game

        # start with first player to log on
        SELECT gp.player_id INTO czar_id FROM game_players gp
                INNER JOIN player p ON gp.player_id = p.id
        WHERE gp.game_id = the_game_id AND gp.is_connected = TRUE
        ORDER BY p.logon_time ASC LIMIT 1;

        SET new_round_num = 1;
        SET new_sub_round_num = 1;

    ELSE # in-game

        SELECT sub_round_num INTO cur_sub_round_num FROM question_history WHERE game_id = the_game_id
        ORDER BY round_num DESC, sub_round_num DESC LIMIT 1;

        # pick next card czar - get candidates (players that haven't czar'ed this round)
        SELECT COUNT(gp.player_id) INTO num_candidates FROM game_players gp
            LEFT JOIN (SELECT qh.card_czar_id AS id FROM question_history qh
                        WHERE qh.game_id = the_game_id AND qh.round_num = cur_round_num) AS past_czars
            ON gp.player_id = past_czars.id
        WHERE past_czars.id IS NULL AND gp.game_id = the_game_id AND gp.is_connected = TRUE;

        IF num_candidates = 0 THEN # start new round

            # start with first player to log on
            SELECT gp.player_id INTO czar_id FROM game_players gp
                INNER JOIN player p ON gp.player_id = p.id
            WHERE gp.game_id = the_game_id AND gp.is_connected = TRUE
            ORDER BY p.logon_time ASC LIMIT 1;

            SET new_round_num = cur_round_num + 1;
            SET new_sub_round_num = 1;

        ELSE # next sub-round

            # player first logged on, excluding players already czar'ed this round
            SELECT excl_czars.player_id INTO czar_id FROM
            ( SELECT gp.player_id FROM game_players gp
                  LEFT JOIN (SELECT qh.card_czar_id AS id FROM question_history qh
                             WHERE qh.game_id = the_game_id AND qh.round_num = cur_round_num) AS past_czars
                            ON gp.player_id = past_czars.id
              WHERE past_czars.id IS NULL AND gp.game_id = the_game_id AND gp.is_connected = TRUE) AS excl_czars

            INNER JOIN player p on excl_czars.player_id = p.id
            ORDER BY p.logon_time ASC LIMIT 1;

            SET new_round_num = cur_round_num;
            SET new_sub_round_num = cur_sub_round_num + 1;

        end if;

    end if;

    # insert question
    INSERT INTO question_history (game_id, question_id, card_czar_id, round_num, sub_round_num)
    VALUES (the_game_id, new_question_id, czar_id, new_round_num, new_sub_round_num);

    COMMIT;

    # output
    SELECT czar_id, new_question_id, new_round_num, new_sub_round_num;

end//
DELIMITER ;
