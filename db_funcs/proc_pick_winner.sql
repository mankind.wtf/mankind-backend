DROP PROCEDURE IF EXISTS pick_winner;
DELIMITER //
CREATE PROCEDURE pick_winner(the_game_id INT, picker_id INT, the_resp_id INT)
BEGIN

    # 0 fine
    # -1 picker not czar; -2 invalid resp id
    DECLARE status_code INT DEFAULT 0;

    DECLARE cur_round INT;
    DECLARE cur_sub_round INT;

    DECLARE the_czar_id INT;
    DECLARE resp_count INT;

    DECLARE winning_player_id INT DEFAULT NULL;
    DECLARE winner_pts INT;

    # get round status
    SELECT round_num, sub_round_num INTO cur_round, cur_sub_round FROM question_history
    WHERE game_id = the_game_id
    ORDER BY round_num DESC, sub_round_num DESC LIMIT 1;

    # check if picker is czar
    SELECT card_czar_id INTO the_czar_id FROM question_history WHERE game_id = the_game_id
        ORDER BY round_num DESC, sub_round_num DESC LIMIT 1;

    IF the_czar_id != picker_id THEN
        SET status_code = -1;

    ELSE # check that resp id is of current round
        SELECT COUNT(id) INTO resp_count FROM response_history WHERE game_id = the_game_id
            AND round_num = cur_round AND sub_round_num = cur_sub_round AND id = the_resp_id;

        IF resp_count = 0 THEN
            SET status_code = -2;
        end if;
    end if;

    IF status_code = 0 THEN

        START TRANSACTION;

        UPDATE question_history SET winner_id = the_resp_id
            WHERE game_id = the_game_id AND round_num = cur_round AND sub_round_num = cur_sub_round;

        UPDATE game SET status = 'IN_GAME_WINNER_PICKED' WHERE id = the_game_id;

        # get some winner info
        SELECT player_id INTO winning_player_id FROM response_history WHERE id=the_resp_id;
        SELECT points INTO winner_pts FROM game_players WHERE game_id = the_game_id AND player_id = winning_player_id;

        # update winner points
        UPDATE game_players SET points = (winner_pts+1) WHERE game_id = the_game_id AND player_id = winning_player_id;

        COMMIT;

    end if;

    # output
    SELECT status_code, winning_player_id;

end//
DELIMITER ;
