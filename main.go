package main

import (
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/globals"
	"gitlab.com/mankind.wtf/mankind-backend/fsociety/routes"
	"log"
	"net/http"
	"os"
)

func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Fsociety backend is running.")
}

func main() {
	// quick command line arg processing
	if len(os.Args) > 1 {
		globals.SetProd(os.Args[1] != "dev")
	}

	// check db
	db := checkDB()
	defer db.Close()

	router := mux.NewRouter().StrictSlash(true) // actually not strict imo

	router.HandleFunc("/", homeHandler)

	for _, routeDef := range routes.GetRouteHandlers() {
		router.HandleFunc(routeDef.Path, routeDef.Handler)
	}

	log.Printf("Server now listening on port %d", globals.SERVER_PORT)
	err := http.ListenAndServe(fmt.Sprintf(":%d", globals.SERVER_PORT), router) // block pretty much forever
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Program successfully finished.")
}

func checkDB() *sql.DB {
	print("Checking DB connection... ")
	db := fsociety.GetDB()

	// This check is only really necessary in dev, where the DB tunnel might not be connected.
	// But skipping this check leads to very difficult-to-interpret errors later on in runtime.
	err := db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	println("OK")

	return db
}
